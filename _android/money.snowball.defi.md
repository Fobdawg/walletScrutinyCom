---
wsId: 
title: 'Snowball: Smart DeFi wallet'
altTitle: 
authors: 
users: 1000
appId: money.snowball.defi
appCountry: 
released: Sep 30, 2020
updated: 2022-06-28
version: 2.6.3
stars: 3.6
ratings: 
reviews: 9
size: 
website: https://www.snowball.money/
repository: 
issue: 
icon: money.snowball.defi.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-06-23
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

