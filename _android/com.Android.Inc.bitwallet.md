---
wsId: BitWallet
title: BitWallet - Buy & Sell Bitcoin
altTitle: 
authors:
- leo
users: 10000
appId: com.Android.Inc.bitwallet
appCountry: 
released: 2019-07-22
updated: 2022-06-03
version: 1.4.23
stars: 4.9
ratings: 1214
reviews: 287
size: 
website: https://www.bitwallet.org
repository: 
issue: 
icon: com.Android.Inc.bitwallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: bitwalletinc
social:
- https://www.facebook.com/BitWalletInc
redirect_from:
- /com.Android.Inc.bitwallet/
- /posts/com.Android.Inc.bitwallet/

---

This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.
