---
wsId: huobi
title: Huobi：Buy Crypto & Bitcoin
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2022-06-11
version: 7.0.4
stars: 4.5
reviews: 2092
size: '405851136'
website: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
